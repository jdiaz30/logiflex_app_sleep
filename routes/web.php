<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/workers', 'WorkerController@index');
Route::post('/workers', 'WorkerController@create');

Route::get('/workers/{workerId}/sleeps', 'SleepController@index');
Route::get('/workers/{workerId}', 'WorkerController@detail');
Route::post('/workers/sleep', 'SleepController@create');
Route::post('/workers/sleep-list', 'SleepController@createList');
Route::put('/workers/{workerId}', 'WorkerController@edit');
Route::post('/workers/{workerId}/associate-device', 'WorkerController@associateDevice');

Route::get('/workers/{workerId}/steps', 'StepController@index');
Route::post('/workers/steps', 'StepController@create');
