<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Services\WorkerService;

class WorkerController extends Controller
{
    protected $_workerService;

    public function __construct(WorkerService $workerService)
    {
        $this->_workerService = $workerService;
    }

    public function index(Request $request)
    {
        $pageSize = (int)$request->input("page_size");
        $workersList = $this->_workerService->getAll($request->input(), $pageSize);
        return response()->json(["status" => 200, "detail" => "Lista de trabajadores", "data" => $workersList], 200);
    }

    public function detail(Request $request, int $workerId)
    {
        $worker = $this->_workerService->getWorker($workerId);
        return response()->json(["status" => 200, "detail" => "Trabajador", "data" => $worker], 200);
    }

    public function create(Request $request)
    {
        return $this->TryCatchDB(function () use ($request) {
            $params = $request->input();
            $response = $this->_workerService->register($params);

            return response()->json(["status" => $response["status"], "detail" => $response["detail"], "data" => $response["data"]], $response["status"]);
        });
    }

    public function edit(Request $request, int $id)
    {
        return $this->TryCatchDB(function () use ($request, $id) {
            $params = $request->input();
            $response = $this->_workerService->update($params, $id);

            return response()->json(["status" => 200, "detail" => "Trabajador actualizado", "data" => ""], 200);
        });
    }

    public function associateDevice(Request $request, int $id)
    {
        return $this->TryCatchDB(function () use ($request, $id) {
            $deviceMac = $request->input("device_mac");
            $response = $this->_workerService->associateDeviceWorker($deviceMac, $id);

            return response()->json(["status" => 200, "detail" => "Dispositivo asociado", "data" => ""], 200);
        });
    }
}
