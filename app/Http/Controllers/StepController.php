<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Services\StepService;

class StepController extends Controller
{
    protected $_stepService;

    public function __construct(StepService $stepService)
    {
        $this->_stepService = $stepService;
    }

    public function index(Request $request, int $workerId)
    {
        $startDate = $request->input("start_date");
        $endDate = $request->input("end_date");

        $stepList = $this->_stepService->getAllByWorker($workerId, $startDate, $endDate);

        return response()->json(["status" => 200, "detail" => "Lista de pasos", "data" => $stepList], 200);
    }

    public function create(Request $request)
    {
        return $this->TryCatchDB(function () use ($request) {
            $params = $request->input();
            $response = $this->_stepService->register($params);

            return response()->json(["status" => $response["status"], "detail" => $response["detail"], "data" => $response["data"]], $response["status"]);
        });
    }
}
