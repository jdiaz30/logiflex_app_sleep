<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Services\SleepService;

class SleepController extends Controller
{
    protected $_sleepService;

    public function __construct(SleepService $sleepService)
    {
        $this->_sleepService = $sleepService;
    }

    public function index(Request $request, int $workerId)
    {
        $startDate = $request->input("start_date");
        $endDate = $request->input("end_date");

        $sleepList = $this->_sleepService->getAllByWorker($workerId, $startDate, $endDate);

        return response()->json(["status" => 200, "detail" => "Lista de actividades", "data" => $sleepList], 200);
    }

    public function create(Request $request)
    {
        return $this->TryCatchDB(function () use ($request) {
            $params = $request->input();
            $response = $this->_sleepService->register($params);

            return response()->json(["status" => $response["status"], "detail" => $response["detail"], "data" => $response["data"]], $response["status"]);
        });
    }

    public function createList(Request $request)
    {
        return $this->TryCatchDB(function () use ($request) {
            $params = $request->input();
            $response = $this->_sleepService->registerList($params);

            return response()->json(["status" => $response["status"], "detail" => $response["detail"], "data" => $response["data"]], $response["status"]);
        });
    }
}
