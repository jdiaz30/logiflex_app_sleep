<?php

namespace App\Services;

use App\Repository\WorkerRepository;

class WorkerService
{
    protected $_workerRep;

    public function __construct(WorkerRepository $workerRepository)
    {
        $this->_workerRep = $workerRepository;
    }

    public function getAll(array $params, int $pageSize)
    {
        return $this->_workerRep->getAll($params, $pageSize);
    }

    public function update(array $data, int $workerId)
    {
        $this->_workerRep->edit($data, $workerId);
    }

    public function getWorker(int $workerId)
    {
        return $this->_workerRep->get($workerId);
    }

    public function associateDeviceWorker(string $deviceMac, int $workerId)
    {
        $workerPrev = $this->_workerRep->exitsByDevice($deviceMac);
        if ($workerPrev && $workerId !== $workerPrev["id"]) {
            $data["device_mac"] = "";
            $this->_workerRep->edit($data, $workerPrev["id"]);
        }

        $data["device_mac"] = $deviceMac;
        $this->_workerRep->edit($data, $workerId);
    }

    public function register(array $data)
    {
        $response = ["detail" => "", "data" => "", "status" => 404];
        $worker = (!empty($data["device_mac"])) ? $this->_workerRep->exitsByDevice($data["device_mac"]) : false;
        if ($worker) {
            $response["detail"] = "Trabajador ya existe";
        } else {
            $response["data"] = $this->_workerRep->register($data);
            $response["detail"] = "Trabajador registrado";
            $response["status"] = 200;
        }

        return $response;
    }
}
