<?php

namespace App\Services;

use Carbon\Carbon;
use App\Repository\WorkerRepository;
use App\Repository\StepRepository;

class StepService
{
    protected $_workerRep;
    protected $_stepRep;

    public function __construct(WorkerRepository $workerRepository, StepRepository $stepRepository)
    {
        $this->_workerRep = $workerRepository;
        $this->_stepRep = $stepRepository;
    }

    public function getAll(array $params, int $pageSize)
    {
        return $this->_stepRep->getAll($params, $pageSize);
    }

    public function getAllByWorker(int $workerId, string $startDate, string $endDate)
    {
        return $this->_stepRep->getAllByWorker($workerId, $startDate, $endDate);
    }

    public function register(array $data)
    {
        $response = ["detail" => "", "data" => "", "status" => 404];
        $worker = $this->_workerRep->exitsByDevice($data["identifier"]);
        if ($worker) {
            $data["worker_id"] = $worker->id;
            $data["date_created"] = Carbon::now()->format("Y-m-d H:i:s");

            $validate = $this->_stepRep->validateRegister($worker->id);
            if ($validate["validate"] == false) {
                $this->_stepRep->register($data);
                $response["detail"] = "Datos de pasos registrado";
                $response["status"] = 200;
            } else {
                $response["data"] = $this->_stepRep->update($worker->id, $data["steps"]);
                $response["detail"] = "Datos de pasos actualizado";
            }
        }

        return $response;
    }
}
