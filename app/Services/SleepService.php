<?php

namespace App\Services;

use Carbon\Carbon;
use App\Repository\WorkerRepository;
use App\Repository\SleepRepository;

class SleepService
{
    protected $_workerRep;
    protected $_sleepRep;

    public function __construct(WorkerRepository $workerRepository, SleepRepository $sleepRepository)
    {
        $this->_workerRep = $workerRepository;
        $this->_sleepRep = $sleepRepository;
    }

    public function getAll(array $params, int $pageSize)
    {
        return $this->_sleepRep->getAll($params, $pageSize);
    }

    public function getAllByWorker(int $workerId, string $starDate, string $endDate)
    {
        return $this->_sleepRep->getAllByWorker($workerId, $starDate, $endDate);
    }

    public function register(array $data)
    {
        $response = ["detail" => "", "data" => "", "status" => 404];
        $worker = $this->_workerRep->exitsByDevice($data["identifier"]);
        if ($worker) {
            $data["start_date"] = $data["sleepIni"];
            $data["end_date"] = $data["sleepEnd"];
            $data["deep_sleep_text"] = $data["deepSleepText"];
            $data["deep_sleep_value"] = $data["deepSleepValue"];
            $data["light_sleep_text"] = $data["lightSleepText"];
            $data["light_sleep_value"] = $data["lightSleepValue"];
            $data["total_sleep_text"] = $data["totalSleepText"];
            $data["total_sleep_value"] = $data["totalSleepValue"];
            $data["worker_id"] = $worker->id;
            $data["date_created"] = Carbon::now()->format("Y-m-d H:i:s");

            $validate = $this->_sleepRep->validateRegister($worker->id, $data["start_date"], $data["end_date"]);
            if ($validate) {
                $this->_sleepRep->register($data);
                $response["detail"] = "Datos de actividad registrado";
                $response["status"] = 200;
            } else {
                $response["detail"] = "Ya existe datos de actividad registrada para el dia de hoy";
            }
        }

        return $response;
    }

    public function registerList(array $devices)
    {
        foreach ($devices as $device) {
            $this->register($device);
        }

        $response = ["detail" => "", "data" => "", "status" => 200];

        return $response;
    }
}
