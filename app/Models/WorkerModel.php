<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkerModel extends Model
{
    protected $table = 'tb_worker';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
