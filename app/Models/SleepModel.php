<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SleepModel extends Model
{
    protected $table = 'tb_sleep';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
