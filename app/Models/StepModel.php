<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StepModel extends Model
{
    protected $table = 'tb_steps';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
