<?php

namespace App\Repository;

use App\Models\StepModel;
use Carbon\Carbon;

class StepRepository
{

    public function __construct()
    { }

    public function register(array $data)
    {
        $step = new StepModel();
        $step->date_created = $data['date_created'];
        $step->steps = $data['steps'];
        $step->worker_id = $data['worker_id'];

        $step->save();
        return $step;
    }

    public function getAll(array $params, $pageSize)
    {
        $query = StepModel::simplePaginate($pageSize);
        return $query;
    }

    public function getAllByWorker(int $workerId, string $startDate, string $endDate)
    {
        $query = StepModel::where("worker_id", "=", $workerId)->whereDate("date_created", ">=", $startDate)->whereDate("date_created", "<=", $endDate)->get();
        return $query;
    }

    public function validateRegister(int $workerId)
    {
        $response = ["validate" => false, "data" => ""];

        $step = StepModel::whereDay("date_created", "=", Carbon::now()->format("d"))
            ->whereMonth("date_created", "=", Carbon::now()->format("m"))
            ->whereYear("date_created", "=", Carbon::now()->format("Y"))
            ->where("worker_id", "=", $workerId)->first();

        if ($step) {
            $response["validate"] = true;
            $response["data"] = $step;
        }

        return $response;
    }

    public function update(int $workerId, int $steps)
    {
        $step = StepModel::where("worker_id", "=", $workerId)->first();
        $step->steps = $steps;

        $step->update();
        return $step;
    }
}
