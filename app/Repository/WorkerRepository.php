<?php

namespace App\Repository;

use App\Models\WorkerModel;


class WorkerRepository
{

    public function __construct()
    { }

    public function register(array $data)
    {
        $worker = new WorkerModel();
        $worker->name = $data['name'];
        $worker->lastname_paternal = $data['lastname_paternal'];
        $worker->lastname_maternal = $data['lastname_maternal'];
        $worker->device_mac = $data['device_mac'];
        $worker->type = $data['type'];
        $worker->model = $data['model'];

        $worker->save();
        return $worker;
    }

    public function get(int $workerId)
    {
        return WorkerModel::where("id", "=", $workerId)->first();
    }

    public function edit(array $data, int $workerId)
    {
        $worker = $this->get($workerId);

        $worker->name = (isset($data['name'])) ? $data['name'] : $worker->name;
        $worker->lastname_paternal = (isset($data['lastname_paternal'])) ? $data['lastname_paternal'] : $worker->lastname_paternal;
        $worker->lastname_maternal = (isset($data['lastname_maternal'])) ? $data['lastname_maternal'] : $worker->lastname_maternal;
        $worker->device_mac = (isset($data['device_mac'])) ? $data['device_mac'] : $worker->device_mac;
        $worker->type = (isset($data['type'])) ? $data['type'] : $worker->type;
        $worker->model = (isset($data['model'])) ? $data['model'] : $worker->model;

        $worker->update();
        return $worker;
    }

    public function exitsByDevice(string $device_mac)
    {
        $worker = WorkerModel::where("device_mac", "=", $device_mac)->first();
        return $worker;
    }

    public function getAll(array $params, $pageSize)
    {
        $query = WorkerModel::where(function ($query) use ($params) {
            if (array_key_exists("name", $params)) {
                $query->orWhere('name', 'like', '%' . $params['name'] . '%');
                $query->orWhere('lastname_paternal', 'like', '%' . $params['name'] . '%');
                $query->orWhere('lastname_maternal', 'like', '%' . $params['name'] . '%');
            }
        })->simplePaginate($pageSize);
        //->toSql();

        //dd($query);

        return $query;
    }
}
