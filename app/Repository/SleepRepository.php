<?php

namespace App\Repository;

use App\Models\SleepModel;
use Carbon\Carbon;

class SleepRepository
{

    public function __construct()
    { }

    public function register(array $data)
    {
        $sleep = new SleepModel();
        $sleep->start_date = $data['start_date'];

        $sleep->deep_sleep_value = $data["deep_sleep_value"];
        $sleep->deep_sleep_text = $data["deep_sleep_text"];

        $sleep->light_sleep_value = $data["light_sleep_value"];
        $sleep->light_sleep_text = $data["light_sleep_text"];

        $sleep->total_sleep_value = $data["total_sleep_value"];
        $sleep->total_sleep_text = $data["total_sleep_text"];

        $sleep->end_date = $data['end_date'];
        $sleep->worker_id = $data['worker_id'];
        $sleep->date_created = $data['date_created'];

        $sleep->save();
        return $sleep;
    }

    public function getAll(array $params, $pageSize)
    {
        $query = SleepModel::simplePaginate($pageSize);
        return $query;
    }

    public function getAllByWorker(int $workerId, string $startDate, string $endDate)
    {
        $query = SleepModel::where("worker_id", "=", $workerId)->whereDate("start_date", ">=", $startDate)->whereDate("end_date", "<=", $endDate)->get();
        return $query;
    }

    public function validateRegister(int $workerId, string $startDate, string $endDate)
    {
        $parseStartDate = Carbon::parse($startDate);
        $parseEndDate = Carbon::parse($endDate);

        $validate = SleepModel::where("worker_id", "=", $workerId)
            ->whereDay("start_date", "=", $parseStartDate->day)
            ->whereMonth("start_date", "=", $parseStartDate->month)
            ->whereYear("start_date", "=", $parseStartDate->year)

            ->whereDay("end_date", "=", $parseEndDate->day)
            ->whereMonth("end_date", "=", $parseEndDate->month)
            ->whereYear("end_date", "=", $parseEndDate->year)
            ->count();
        return $validate > 0 ? false : true;
    }
}
